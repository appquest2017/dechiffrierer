package ch.appquest.dechiffrierer;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;

import java.util.Observable;

public class ImageProcessingBackground extends Observable {
    private int color = Color.RED;

    public ImageProcessingBackground(Bitmap bitmap) {
        new Background().execute(bitmap);
    }

    public ImageProcessingBackground(Bitmap bitmap, int color) {
        this.color = color;
        new Background().execute(bitmap);
    }

    class Background extends AsyncTask<Bitmap, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(Bitmap... bitmaps) {
            return ImageProcessing.processBitmap(bitmaps[0], color);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            setChanged();
            notifyObservers(bitmap);
        }
    }

}
