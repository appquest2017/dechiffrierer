package ch.appquest.dechiffrierer;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;
import com.polites.android.GestureImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class MainActivity extends AppCompatActivity implements Observer {
    private static int SCAN_QR_CODE_REQUEST_CODE = 0, REQUEST_IMAGE_CAPTURE = 1;

    Context context = this;
    private Button btnOpenCamera, btnSolution;
    private TextView lblRGBValue;
    private PermissionListener permissionlistener;
    private GestureImageView imgResult;
    private LinearLayout root;

    private Bitmap current_bitmap = null;

    private SeekBar barColorRed, barColorGreen, barColorBlue;

    private int processed_times = 0;

    /**
     * On activity creation.
     * @param savedInstanceState Saved state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize controls
        btnOpenCamera = (Button) findViewById(R.id.btnCamera);
        btnSolution = (Button) findViewById(R.id.btnSolution);
        barColorRed = (SeekBar) findViewById(R.id.barColorRed);
        barColorGreen = (SeekBar) findViewById(R.id.barColorGreen);
        barColorBlue = (SeekBar) findViewById(R.id.barColorBlue);
        lblRGBValue = (TextView) findViewById(R.id.lblRGBValue);
        root = (LinearLayout) findViewById(R.id.root);

        //Set colors to seekbars
        barColorRed.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY));
        barColorRed.getThumb().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);

        barColorGreen.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY));
        barColorGreen.getThumb().setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_IN);

        barColorBlue.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.BLUE, PorterDuff.Mode.MULTIPLY));
        barColorBlue.getThumb().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

        // Create click listener for camera button.
        btnOpenCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TedPermission.with(MainActivity.this)
                        .setPermissionListener(permissionlistener)
                        .setDeniedMessage("If you reject permission,you can not use this service\n\nPlease turn on permissions at [Setting] > [Permission]")
                        .setPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .check();
            }
        });

        // Create click listener for solution input button.
        btnSolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder inputAlert = new AlertDialog.Builder(context);
                inputAlert.setTitle("Solution word");
                inputAlert.setMessage("Enter the word from the image (case-sensitive!).");
                final EditText solutionInput = new EditText(context);
                inputAlert.setView(solutionInput);
                inputAlert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        log(solutionInput.getText().toString());
                    }
                });
                inputAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alertDialog = inputAlert.create();
                alertDialog.show();
            }
        });

        initalizeSeekbars();

        // Create permission listener for camera and storage.
        permissionlistener = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                dispatchTakePictureIntent();
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {
                Toast.makeText(MainActivity.this, "Permission Denied\n" + deniedPermissions.toString(), Toast.LENGTH_SHORT).show();
            }
        };
    }

    private void initalizeSeekbars() {
        barColorRed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                changeLblRGBValue(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                requestWithNewColor();
            }
        });
        barColorGreen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                changeLblRGBValue(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                requestWithNewColor();
            }
        });
        barColorBlue.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                changeLblRGBValue(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                requestWithNewColor();
            }
        });
    }

    private void changeLblRGBValue(int i) {
        lblRGBValue.setText(i+"");
    }

    private void requestWithNewColor() {
        if(current_bitmap!=null) {
            int color = Color.argb(255,barColorRed.getProgress(),barColorGreen.getProgress(),barColorBlue.getProgress());
            (new ImageProcessingBackground(current_bitmap, color)).addObserver(MainActivity.this);
        }
    }

    /**
     * Callback for intends.
     * @param requestCode Request code to differentiate requests.
     * @param resultCode Result code to check what we got
     * @param intent Intend
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                current_bitmap = ImageCreation.readImageFile(Uri.parse(ImageCreation.mCurrentPhotoPath));
                //Start image processing / Notify via observer
                (new ImageProcessingBackground(current_bitmap)).addObserver(this);
            } else {
                // TODO Show error if something went wrong. User-friendlier!
            }
        }
    }

    /**
     * Create log entry in LogBook.
     * @param solution The solution from the alert dialog.
     */
    private void log(String solution) {
        Intent intent = new Intent("ch.appquest.intent.LOG");

        if (getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY).isEmpty()) {
            Toast.makeText(this, "Logbook App not Installed", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            // Create JSON
            JSONObject jsonResult = new JSONObject();
            jsonResult.put("task", "Dechiffrierer");
            jsonResult.put("solution", solution);
            intent.putExtra("ch.appquest.logmessage", jsonResult.toString());
        } catch (JSONException ex) {
            Toast.makeText(this, "Error while creating JSON", Toast.LENGTH_LONG).show();
            return;
        }

        startActivity(intent);
    }

    /**
     * Open camera app to take a picture.
     */
    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = ImageCreation.createImageFile(this);
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.example.android.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    /**
     * Callback for image processing task.
     * @param observable The observable class
     * @param o Return object
     */
    @Override
    public void update(Observable observable, Object o) {
        if (observable instanceof ImageProcessingBackground) {
            Bitmap bitmap = null;
            try {
                bitmap = (Bitmap) o;
            } catch (ClassCastException ex) {
                // Ignored
            }
            if(processed_times!=0) {
                root.removeViewAt(0);
            }
            imgResult = new GestureImageView(this);
            imgResult.setImageBitmap(bitmap);
            imgResult.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1f));

            root.addView(imgResult,0);

            processed_times++;
        }
        Toast.makeText(this, "Finished image processing", Toast.LENGTH_SHORT).show();
    }
}
