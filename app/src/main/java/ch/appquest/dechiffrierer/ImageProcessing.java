package ch.appquest.dechiffrierer;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Shader;

import java.util.Observable;

class ImageProcessing extends Observable {

    static Bitmap processBitmap(Bitmap bitmap){
        return processBitmap(bitmap, Color.RED);
    }

    static Bitmap processBitmap(Bitmap bitmap, int color) {
        System.out.println("Width: " + bitmap.getWidth() + ", Height: " + bitmap.getHeight());

        // Create mutable copy of the original image to save changes
        Bitmap result = bitmap.copy(Bitmap.Config.ARGB_8888, true);

        // Create a fully red bitmap
        Bitmap redBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(redBitmap);
        Paint paint = new Paint();
        paint.setColor(color);
        canvas.drawRect(0, 0, bitmap.getWidth(), bitmap.getHeight(), paint);

        // Create a filter from the red bitmap
        Paint p = new Paint();
        p.setXfermode(new PorterDuffXfermode((PorterDuff.Mode.MULTIPLY)));
        p.setShader(new BitmapShader(redBitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));

        // Apply the filter to the bitmap, save everything, and return the image.
        Canvas resCanvas = new Canvas();
        resCanvas.setBitmap(result);
        resCanvas.drawBitmap(bitmap, 0, 0, null);
        resCanvas.drawRect(0, 0, bitmap.getWidth(), bitmap.getHeight(), p);
        return result;
    }
}
